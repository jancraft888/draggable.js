![draggable.js banner](/assets/draggablejs_banner.png)

# draggable.js
Simple and lightweight JS element drag utility.

## Usage
Just add a \<script\> tag with the source file in your HTML:
```html
<script src="draggable.min.js"></script>
```

Then use it like in this example:
```html
<div id="drag-surface"></div>
<script src="draggable.min.js"></script>
<script>
    const surface = new DraggableSurface("#drag-surface", { grid: 16 })
    
    for (let i = 0; i < 5; i++) {
        const el = document.createElement('div')
        el.classList.add('drag-element')
        el.innerHTML = `${i}`
        
        surface.addDraggableElement(el)
    }
    surface.addEventListener('elementdragged', (ev) => {
        console.log(ev)
    })
</script>
```

## Documentation
`new DraggableSurface(selector, options)`<br>
Creates a draggable surface that allows elements to be dragged within it.<br>
Options:
 - *grid* locks dragged elements to a grid of size **n**
 - *scrollRange* makes the edge of the screen in a range **n** automatically scroll
 - *checkDepth* is used to check up to the **n**th parent of the clicked element for a draggable element.
 - *clickThreshold* is the maximum time in milliseconds that registers as a click instead of a drag


`DraggableSurface.addDraggableElement(el, x, y)`<br>
Adds an element to the draggable surface, located at **x** and **y** relative to the surface.


`DraggableSurface.addEventListener(type, listener, options)`<br>
Adds the desired event listener to the DraggableSurface element, can be used to add an *elementdragged* event listener.


`ElementDraggedEvent`<br>
The *elementdragged* event is fired on the dragged element and bubbles up through the DOM, which means you can process it on the DraggableSurface. It has 3 notable properties:
 - **target** is the element that was dragged
 - **detail.x** is the X coordinate *('left' CSS property)* of the dragged element
 - **detail.y** is the Y coordinate *('top' CSS property)* of the dragged element


`ElementClickedEvent`<br>
The *elementclicked* event is fired on the clicked element and bubbles up through the DOM, which means you can process it on the DraggableSurface. It has one notable property:
 - **target** is the element that was clicked

