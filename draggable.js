class DraggableSurface {
    constructor(selector, opt={}) {
        this.el = document.querySelector(selector)
        this.el.style.position = 'relative'
        this.elements = []
        this.currentDrag = null
        this.offX = 0
        this.offY = 0
        this.grid = opt.grid ?? 1
        this.scrollRange = opt.scrollRange ?? -1
        this.checkDepth = opt.checkDepth ?? 0
        this.clickThreshold = opt.clickThreshold ?? 100
        this.clickTime = 0

        const _pxi = (pxs) => {
            return parseInt(pxs.substr(0, pxs.length - 2))
        }

        const startdrag = (x, y) => {
            const topel = document.elementFromPoint(x, y)
            const checkels = [ topel ]
            for (let i = 0; i < this.checkDepth; i++) {
                checkels.push(checkels[checkels.length - 1].parentElement)
            }
            for (const el of checkels)
                if (this.elements.includes(el)) {
                    this.offX = _pxi(el.style.left) - x
                    this.offY = _pxi(el.style.top) - y
                    el.style.zIndex = 1
                    this.currentDrag = el
                    this.clickTime = Date.now()
                    break
                }
        }

        const enddrag = () => {
            if (this.currentDrag) {
                if (Date.now() - this.clickTime < this.clickThreshold) {
                    this.currentDrag.dispatchEvent(new CustomEvent('elementclicked', { bubbles: true }))
                    this.currentDrag = null
                    return
                }
                let x = _pxi(this.currentDrag.style.left)
                let y = _pxi(this.currentDrag.style.top)
                if (this.grid > 1) {
                    x = Math.round(x / this.grid) * this.grid
                    y = Math.round(y / this.grid) * this.grid
                }
                this.currentDrag.style.top = `${y}px`
                this.currentDrag.style.left = `${x}px`

                const ev = new CustomEvent('elementdragged', {
                    bubbles: true,
                    detail: { x, y }
                })
                this.currentDrag.style.zIndex = ""
                this.currentDrag.dispatchEvent(ev)
            }
            this.currentDrag = null
        }

        const movedrag = (_x, _y) => {
            let x = _x + this.offX
            let y = _y + this.offY
            if (x < 0) x = 0
            if (x > this.el.clientWidth - this.currentDrag.clientWidth) x = this.el.clientWidth - this.currentDrag.clientWidth
            if (y < 0) y = 0
            if (y > this.el.clientHeight - this.currentDrag.clientHeight) y = this.el.clientHeight - this.currentDrag.clientHeight

            this.currentDrag.style.top = `${y}px`
            this.currentDrag.style.left = `${x}px`
        }

        this.el.addEventListener('mousemove', ev => {
            if (!this.currentDrag) return
            let x = ev.x;
            let y = ev.y;

            if (ev.clientX < this.scrollRange) {
                window.scrollBy(ev.clientX - this.scrollRange, 0)
                this.offX += ev.clientX - this.scrollRange
            }
            if (ev.clientX > window.innerWidth - this.scrollRange) {
                window.scrollBy(ev.clientX - (window.innerWidth - this.scrollRange), 0)
                this.offX += ev.clientX - (window.innerWidth - this.scrollRange)
            }
            if (ev.clientY < this.scrollRange) {
                window.scrollBy(0, ev.clientY - this.scrollRange)
                this.offY += ev.clientY - this.scrollRange
            }
            if (ev.clientY > window.innerHeight - this.scrollRange) {
                window.scrollBy(0, ev.clientY - (window.innerHeight - this.scrollRange))
                this.offY += ev.clientY - (window.innerHeight - this.scrollRange)
            }
            if (x + this.offX < 0) this.offX = -x
            if (y + this.offY < 0) this.offY = -y
            if (x + this.offX > this.el.clientWidth - this.currentDrag.clientWidth) this.offX = -x + (this.el.clientWidth - this.currentDrag.clientWidth)
            if (y + this.offY > this.el.clientHeight - this.currentDrag.clientHeight) this.offY = -y + (this.el.clientHeight - this.currentDrag.clientHeight)
            
            movedrag(x, y)
        })
        this.el.addEventListener('mousedown', ev => {
          startdrag(ev.x, ev.y)
        })
        this.el.addEventListener('mouseup', () => {
            enddrag()
        })
        this.el.addEventListener('touchstart', ev => {
            startdrag(ev.touches[0].clientX, ev.touches[0].clientY)
        })
        this.el.addEventListener('touchmove', ev => {
            if (!this.currentDrag) return

            movedrag(ev.touches[0].clientX, ev.touches[0].clientY)
        })
        this.el.addEventListener('touchend', () => {
            enddrag()
        })
    }

    addDraggableElement(el, x=0, y=0) {
        el.style.position = 'absolute'
        el.style.top = `${y}px`
        el.style.left = `${x}px`

        this.elements.push(el)
        this.el.appendChild(el)
    }

    addEventListener(type, listener, options) {
        return this.el.addEventListener(type, listener, options)
    }
}
